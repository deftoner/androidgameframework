package com.javiersan.androidgames.mrnom;

import com.javiersan.androidgames.framework.Screen;
import com.javiersan.androidgames.framework.impl.AndroidGame;

public class MrNomGame extends AndroidGame {
	public Screen getStartScreen() {
		return new LoadingScreen(this);
	}

}
